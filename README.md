# Debian - Nginx

## Debian

* Installer Debian avec SSH server + Standard system utilities
* Installer GRUB boot loader

### Mise à jour du Debian

```
#!shell
apt-get update
```

```
#!shell
apt-get upgrade
```

### Configuration du réseau

###### Configurer une IP fixe :

```
#!shell
nano /etc/network/interfaces
```

```
#!shell
iface eth0 inet static
        address 192.168.1.100
        netmask 255.255.255.0
        gateway 192.168.1.1
```

###### Redémarrer l'interface réseau :

```
#!shell
/etc/init.d/networking restart
```

###### Editer le fichier hosts :

```
#!shell
nano /etc/hosts
```

```
#!shell
127.0.0.1       localhost.localdomain   	localhost
192.168.1.100   servername.expansion.be     servername
```

###### Exécuter :
```
#!shell
echo servername.expansion.be > /etc/hostname
```

```
#!shell
/etc/init.d/hostname.sh start
```


###### Ensuite :
```
#!shell
hostname
```

```
#!shell
hostname -f
```

Les deux commandes doivent afficher **servername.expansion.be**

###### Synchroniser l'horloge du serveur

```
#!shell
apt-get install ntp ntpdate
```

###### Empêcher le login root en SSH

```
#!shell
nano /etc/ssh/sshd_config
```

```
#!shell
PermitRootLogin no
```

```
#!shell
/etc/init.d/sshd restart
```

###### Rkhunter - Anti-rootkit

```
#!shell
apt-get install rkhunter
```

###### ClamAV - Antivirus

```
#!shell
apt-get install clamav clamav-freshclam
```

```
#!shell
freshclam
```

###### Jailkit - Chrooter les utilisateurs SSH dans leur répertoire de base

```
#!shell
apt-get install build-essential autoconf automake libtool flex bison debhelper binutils-gold
```

```
#!shell
wget http://olivier.sessink.nl/jailkit/jailkit-2.17.tar.gz
tar -vxzf jailkit-2.17.tar.gz
cd jailkit-2.17/
./debian/rules binary
dpkg -i jailkit_2.17-1_amd64.deb
rm -rf jailkit-*
```

###### Fail2ban - Ban les IP qui ont obtenu un trop grand nombre d'échecs lors de l'authentification

```
#!shell
apt-get install fail2ban
```

```
#!shell
nano /etc/fail2ban/jail.conf
```

```
#!shell
ignoreip = 127.0.0.1 192.168.1.0/255 192.168.0.0/255
```



* * *
## MySQL

```
#!shell
apt-get install mysql-server
```

###### Activation :

```
#!shell
mysql_install_db
```

###### Configuration :

```
#!shell
/usr/bin/mysql_secure_installation
```

* Ne pas changer le mot de passe root
* Répondre "Yes" à toutes les questions



* * *
## Nginx

```
#!shell
apt-get install nginx
```

```
#!shell
service nginx start
```


### Configuration

```
#!shell
nano /etc/nginx/sites-available/default
```

```
#!shell
server {
        listen   80;
     

        root /usr/share/nginx/www;
        index index.php index.html index.htm;

        server_name expansion.be;

        location / {
                try_files $uri $uri/ /index.html;
        }

        error_page 404 /404.html;

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
              root /usr/share/nginx/www;
        }

        # pass the PHP scripts to FastCGI server listening on /var/run/php5-fpm.sock
        location ~ \.php$ {
                try_files $uri =404;
                fastcgi_pass unix:/var/run/php5-fpm.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;
                
        }

}
```

* Ajouter index.php à la ligne `index index.html index.htm;`
* Changer la variable `server_name` par le nom de domaine
* Décommenter ou ajouter les lignes relatives à PHP

### Optimisation

```
#!shell
nano /etc/nginx/nginx.conf
```

```
#!shell
worker_processes 1;
worker_connections 1024;
```

```
#!shell
client_body_buffer_size 10K;
client_header_buffer_size 1k;
client_max_body_size 8m;
large_client_header_buffers 2 1k;
client_body_timeout 12;
client_header_timeout 12;
keepalive_timeout 15;
send_timeout 10;
```

```
#!shell
gzip             on;
gzip_comp_level  2;
gzip_min_length  1000;
gzip_proxied     expired no-cache no-store private auth;
gzip_types       text/plain application/x-javascript text/xml text/css application/xml;
```


* * *
## PHP

```
#!shell
apt-get install php5-fpm php5-mysql
```

###### Configuration :

```
#!shell
nano /etc/php5/fpm/php.ini
```

```
#!shell
cgi.fix_pathinfo=0
```

###### Ensuite :

```
#!shell
nano /etc/php5/fpm/pool.d/www.conf
```

```
#!shell
listen = /var/run/php5-fpm.sock
```

###### Redémarrer les services :

```
#!shell
service php5-fpm restart
```

```
#!shell
service nginx restart
```